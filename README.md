This repository has the poster I presented in the COMPSTAT 2018 held in Iași. You can find more information about that 
event in http://www.compstat2018.org.

The title of the presentation was: «Bayesian modeling of individual growth variability using back-calculation: Application 
to pink cusk-eel (Genypterus blacodes) off Chile».

The abstract of my contribution is the following:

«The von Bertalanffy growth function (vBGF) with random effects has been widely used to estimate growth parameters 
incorporating individual variability of length-at-age. Trajectories of individual growth can be inferred using either 
mark-recapture or back-calculation of length-at-age from growth marks in hard body parts such as otoliths. Modern 
statistical methods evaluate individual variation usually from mark-recapture data, and the parameters describing this 
function are estimated using empirical Bayes methods assuming Gaussian error. In this investigation, we combine recent 
studies in non-Gaussian distributions and a Bayesian approach to model growth variability using back-calculated data in 
harvested fish populations. We presumed that errors in the vBGF can be assumed as a Student-$t$ distribution, given the 
abundance of individuals with extreme length values. The proposed method was applied and compared to the standard methods 
using back-calculated length-at-age data for pink cusk-eel ({\itshape Genypterus blacodes}) off Chile. We have found that 
males grow significantly faster than females, and that length-at-age for both sexes exhibits extreme length observations.»

This presentation was entirely based on the article « Bayesian modeling of individual growth variability using 
back-calculation: Application to pink cusk-eel (Genypterus blacodes) off Chile
» (https://www.sciencedirect.com/science/article/abs/pii/S0304380018302370).

